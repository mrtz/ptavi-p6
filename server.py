# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """Clase (y programa principal) para un servidor de eco en UDP simple."""

    def audio(self):
        """Clase (y programa principal)."""
        BIT = secrets.randbelow(1)
        RTP_header = simplertp.RtpHeader()
        RTP_header.set_header(version=2, marker=BIT,
                              payload_type=14, ssrc=200002)
        audio = simplertp.RtpPayloadMp3(AUDIO_)
        simplertp.send_rtp_packet(RTP_header, audio, '127.0.0.1', 23032)

    def handle(self):
        """Clase (y pro."""
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente.
            line = self.rfile.read()
            print("Peticion " + line.decode('utf-8'))
            # Si no hay más líneas salimos del bucle infinito.
            if not line:
                break

            met = line.decode('utf-8').split(' ')[0]
            print("El cliente nos manda " + line.decode('utf-8'))

            if met == 'INVITE':

                try:
                    li = line.decode('utf-8')
                    o = li.split(':')[1].split('@')[0]

                    p = 'SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r\n'
                    p += ' SIP/2.0 200 OK\r\n\r\n'

                    SDP_ = '\r\n\r\nv=0\r\n' + 'o=' + o + '@' + \
                           '127.0.0.1\r\n' + 's= mrtzsession\r\n'
                    SDP_ += 't=0\r\n' + 'm=audio 67876 RTP\r\n\r\n'

                    ct = 'Content-Type: application/sdp'

                    self.wfile.write(bytes(p + ct + SDP_, 'utf-8') + b'\r\n')
                except IndexError:
                    self.wfile.write(b'SIP/2.0 400 Bad Requestr\n')

            elif met == 'BYE':
                self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
            elif met == 'ACK':
                self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
                self.audio()
            else:
                self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n\r\n')


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos

    try:
        IP_ = sys.argv[1]
        PORT_ = int(sys.argv[2])
        AUDIO_ = sys.argv[3]
        # serv = socketserver.UDPServer((IP_, PORT_), EchoHandler)
    except IndexError:
        sys.exit('Usage: python3 server.py IP port audio_file')
    serv = socketserver.UDPServer((IP_, PORT_), EchoHandler)
    print('listening...')

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print('terminado')
