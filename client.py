#!/usr/bin/python3.
# -*- coding: utf-8 -*-.
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socket
import sys

if __name__ == "__main__":
    # hola.
    try:
        METDO = sys.argv[1]
        USU = sys.argv[2].split('@')
        RECEIVER = USU[0]
        SERVI_ = USU[1].split(':')[0]
        PUERT_ = int(USU[1].split(':')[1])

    except IndexError:
        h = 'receiver@IPreceptor:puertoSIP'
        sys.exit('usage: python3 client.py method' + h)
    except ValueError:
        sys.exit("SIP/2.0 400 Bad Request")

    if METDO == 'INVITE' or 'BYE':
        li = METDO + ' Sip: ' + RECEIVER + '@' + SERVI_ + 'SIP/2.0\r\n'

    else:
        li = '  '

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect(('localhost', PUERT_))

    print("Enviando: " + li)
    my_socket.send(bytes(li, 'utf-8') + b'\r\n\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

    d = data.decode('utf-8').split()
    if METDO == 'INVITE':
        if d[1] == " 100":
            if d[4] == " 180 ":
                if d[7] == " 200 ":
                    METDO = 'ACK'
                    ack = (METDO + ' Sip: ' + RECEIVER + '@' + SERVI_
                           + 'SIP/2.0\r\n')
                    my_socket.send(bytes(ack, 'utf-8') + b'\r\n\r\n')
    print("Terminando socket...")
print("Fin.")
